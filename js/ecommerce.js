'use strict';

function detectElementInWindow($element) {
    if (!$element.length) {
        return false;
    }

    if ($element.offset().top + ($element.height()/3) >= $(document).scrollTop()
        && $element.offset().top + ($element.height()/3) <= $(document).scrollTop() + $(window).height()) {
        return true;
    } else {
        return false;
    }
};

function ecommerceGetOrder(id) {
    var d = $.Deferred();

    $.ajax({
        type: "POST",
        url: '/api/get-order/'+id,
        data: {},
        dataType: 'json',
        success: function(response) {
            d.resolve(response);
        }
    });

    return d.promise();
};

function ecommerceGetCartProducts(products) {
    if (typeof products === 'undefined') {
        return false;
    }

    var ecommerceProducts = [];

    for(var key in products) {
        var tempData = {};

        tempData.id = products[key].id;
        tempData.name = products[key].name || products[key].title;
        tempData.price = products[key].price;
        tempData.category = products[key].category;
        // tempData.list = products[key].list;
        tempData.list = window['ecommerceList']['catalog'];
        tempData.quantity = products[key].quantity || products[key].qty;

        ecommerceProducts.push(tempData);
    }

    return ecommerceProducts;
};

function ecommerceGetProductData($product, currentSlide) {
    var $slider = $product.closest('.slick-slider'),
        id = parseInt($product.find("span.productIdStatGA").html()),
        name = $product.find("span.productNameStatGA").html(),
        price = parseInt($product.find("span.productPriceStatGA").html()),
        category = $product.find("span.catTitleStatGA").html(),
        list = $product.find("span.productListStatGA").html(),
        pos = 1,
        needPos;

    if (typeof currentSlide !== 'undefined') { //for slick slider
        needPos = currentSlide + 1;
    } else if ($slider.length) { //for slick slider
        $slider.find('.slick-slide').each(function() {
            if( $(this).attr("aria-hidden") === "false" ) {
                var $tempId = parseInt($(this).find("span.productIdStatGA").html());
                if($tempId === id) {
                    needPos = pos;
                    return;
                }
                pos++;
            }
        });
    } else { //for catalog
        needPos = $product.index() + 1;
    }

    var productsGA = [],
        item = {
            'id': id,
            'name': name,
            'price': price,
            'category': category,
            'list': window['ecommerceList'][list],
            'position': needPos
        };

    productsGA.push(item);

    return productsGA;
};

function ecommerceSliderShow($slider, position) {
    if (typeof window['dataLayer'] === 'undefined' || !$slider.length || !detectElementInWindow($slider)) {
        return false;
    }

    var $current_slide = $slider.find('.slick-active'),
        current_slide_url = $current_slide.find('a').attr('href'),
        current_slide_src = $current_slide.find('.lazy-slider-img').attr('src'),
        productsGA = [],
        item = {},
        src_arr = [],
        id_arr = [],
        id;

    src_arr = current_slide_src.split('/');
    if (src_arr.length) {
        id_arr = src_arr[src_arr.length-1].split('.');
        id = id_arr[0];
    }

    //Топ слайдер + баннер товар
    if (position === 'slider-top-1' && current_slide_url.indexOf('product') !== -1) {
        return false;
    }

    item['id'] = id;
    item['position'] = position;
    productsGA.push(item);

    window['dataLayer'].push({
        'event': 'promotionView',
        'ecommerce': {
            'promoView': productsGA
        }
    });
    console.log('promotionView', productsGA);
};

function ecommerceProductShow($product, currentSlide) {
    if (typeof window['dataLayer'] === 'undefined' || !$product.length) {
        return false;
    }

    var productsGA = ecommerceGetProductData($product, currentSlide);

    window['dataLayer'].push({
        'event': 'Impressions',
        'ecommerce': {
            'currencyCode': 'UAH',
            'impressions': productsGA
        }
    });
    console.log('Impressions', productsGA);
};

function ecommerceCatalogShow($catalog) {
    if (typeof window['dataLayer'] === 'undefined' || !$catalog.length) {
        return false;
    }

    var pushed_array, //масив позиций товаров, которые уже были отправлены в ecommerce
        $products = $catalog.find('.product-item');

    if (!$products.length) {
        return false;
    }

    if ($catalog.data('pushed-array')) {
        pushed_array = JSON.parse($catalog.data('pushed-array'));
    } else {
        pushed_array = [];
    }

    $products.each(function (index, element) {
        var $element = $(element);

        if (detectElementInWindow($element) && pushed_array.indexOf(index) === -1) {//отправляем каждый товар не больше 1-го раза
            ecommerceProductShow($element, index);
            pushed_array.push(index);
        }
    });

    $catalog.data('pushed-array', JSON.stringify(pushed_array));
};

function ecommerceUpdateCart(id, data, response) {
    if (typeof window['dataLayer'] === 'undefined') {
        return false;
    }

    var productGA = {
        'id': id,
        'name': response.productGA.name,
        'price': response.productGA.price,
        'category': response.productGA.category,
        'list': data.list || window['ecommerceList']['catalog'],
        'quantity': response.productGA.quantity
    };

    if (parseInt(data.qty) === 0) { //remove from cart
        productGA['quantity'] = data.current_qty;
    }

    for (var key in response.productContains) {
        response.productContains[key].list =  data.list;
    }

    response.productContains.unshift(productGA);

    var productsGA = response.productContains;

    if(data.action === "increment") {
        window['dataLayer'].push({
            'event': 'addToCart',
            'ecommerce': {
                'currencyCode': 'UAH', // Валюта
                'add': {
                    'products': productsGA
                }
            }
        });
        console.log('addToCart', productsGA);
    }

    if(data.action === "decrement" || parseInt(data.qty) === 0) {
        window['dataLayer'].push({
            'event': 'removeFromCart',
            'ecommerce': {
                'currencyCode': 'UAH', // Валюта
                'remove': {
                    'products': productsGA
                }
            }
        });
        console.log('removeFromCart', productsGA);
    }
};

function ecommerceSubmitCheckout(state) {
    if (typeof window['dataLayer'] === 'undefined'
        || typeof state === 'undefined'
        || typeof state.cart === 'undefined'
        || typeof state.order === 'undefined'
        || typeof state.cart.products === 'undefined' ) {
        return false;
    }

    var cart = state.cart,
        products = cart.products,
        order = state.order,
        ecommerceProducts,
        ecommerceDeliveryType,
        ecommercePaymentType;

    //products
    ecommerceProducts = ecommerceGetCartProducts(products);
    if (!ecommerceProducts) {
        return false;
    }
    //products

    //delivery type
    if (order.delivery_type === 'pickup') {
        ecommerceDeliveryType = 'Забрать самому';
    } else if (order.delivery_type === 'courier') {
        ecommerceDeliveryType = 'Доставка курьером';
    } else {
        ecommerceDeliveryType = '';
    }
    //delivery type

    //payment type
    for(let key in state.payment_methods) {
        if ( parseInt(state.payment_methods[key].id) === parseInt(order.payment_type) ) {
            ecommercePaymentType = state.payment_methods[key].name;
        }
    }
    //payment type

    window['dataLayer'].push({
        'event': 'checkoutStep1',
        'ecommerce': {
            'currencyCode': 'UAH',
            'checkout': {
                'actionField': {
                    'step': 1,
                    'option': ecommerceDeliveryType
                },
                'products': ecommerceProducts
            }
        }
    });

    window['dataLayer'].push({
        'event': 'checkoutStep2',
        'ecommerce': {
            'currencyCode': 'UAH',
            'checkout': {
                'actionField': {
                    'step': 2,
                    'option': ecommercePaymentType
                },
                'products': ecommerceProducts
            }
        }
    });

    console.log('ecommerceProducts: ', ecommerceProducts);
    console.log('ecommerceDeliveryType: ', ecommerceDeliveryType);
    console.log('ecommercePaymentType: ', ecommercePaymentType);
};

function ecommerceSendTransactionCheckout() {
    if (typeof window['dataLayer'] === 'undefined') {
        return false;
    }

    var id_order = getCookie('checkout_id_order');

    if (!parseInt(id_order)) {
        return false;
    }

    ecommerceGetOrder(parseInt(id_order))
        .then(function (response) {
            if (!response) {
                return false;
            }

            var data = response;

            if (data.status && data.status === 'success') {
                var ecommerceProducts = ecommerceGetCartProducts(data.products);

                if (!ecommerceProducts) {
                    return false;
                }

                window['dataLayer'].push({
                    'event': 'transaction',
                    'ecommerce': {
                        'currencyCode': 'UAH',
                        'purchase': {
                            'actionField': {
                                'id': id_order,
                                'affiliation': 'Mafia Online',
                                'revenue': data.total,
                                'shipping': 0,
                                'tax': 0,
                                'coupon': ''
                            },
                            'products': ecommerceProducts
                        }
                    }
                });

                console.log(id_order, data.total, ecommerceProducts);
                setCookie('checkout_id_order', 0, {
                    path: '/'
                });

            }
        });
};

function ecommerceSliderClick($slider) {
    if (typeof window['dataLayer'] === 'undefined') {
        return false;
    }

    var $current_slide = $slider.find('.slick-active'),
        current_slide_url = $current_slide.find('a').attr('href'),
        current_slide_src = $current_slide.find('.lazy-slider-img').attr('src'),
        src_arr = [],
        id_arr = [],
        id,
        position,
        productsGA = [],
        item = {};

    src_arr = current_slide_src.split('/');
    if (src_arr.length) {
        id_arr = src_arr[src_arr.length-1].split('.');
        id = id_arr[0];
    }

    if ($slider.closest('#lazy-slider-section').length) {
        position = 'slider-top-1';
    } else if ($slider.closest('#lazy-banner-section').length) {
        position = 'slider-bottom-1';
    }

    //Топ слайдер + баннер товар
    if (position === 'slider-top-1' && current_slide_url.indexOf('product') !== -1) {
        return false;
    }

    item['id'] = id;
    item['position'] = position;
    productsGA.push(item);

    window['dataLayer'].push({
        'event': 'promotionClick',
        'ecommerce': {
            'promoClick': productsGA
        }
    });
    console.log(productsGA);
};

function ecommerceProductClick($product) {
    if (typeof window['dataLayer'] === 'undefined') {
        return false;
    }

    var productsGA = ecommerceGetProductData($product);

    window['dataLayer'].push({
        'event': 'productClick',
        'ecommerce': {
            'currencyCode': 'UAH',
            'click': {
                'products': productsGA
            }
        }
    });
    console.log('productClick', productsGA);
};

function ecommerceSendSubscribe() {
    if (typeof window['dataLayer'] === 'undefined') {
        return false;
    }

    window['dataLayer'].push({ 'event': 'UAEvent', 'evenCategory': 'Подписка на рассылку' });
};

function ecommerceInit() {

    //Главная страница (топ слайдер и нижний баннер)
    $(document).on('click', '#lazy-slider-section .lazy-slider-img, #lazy-banner-section .lazy-slider-img', function (e) {
        //e.preventDefault();

        var $slider = $(this).closest('.slick-slider');
        ecommerceSliderClick($slider);
    });
    //Главная страница (топ слайдер и нижний баннер)

    //Микро описание товара
    $(document).on('click', '.product-item .product-title, .product-item .product-img', function (e) {
        //e.preventDefault();

        var $product = $(this).closest('.product-item');
        ecommerceProductClick($product);
    });
    //Микро описание товара

    //Мы рекомендуем на главной
    var $productsSlider = $('.products-slider');
    if($productsSlider.length) {
        if ($(window).width() >= 766) { //desktop
            $productsSlider.on('afterChange', function (event, slick, currentSlide, nextSlide) {
                if (detectElementInWindow($productsSlider)) {
                    var $product = $productsSlider.find('.slick-active .product-item');
                    ecommerceProductShow($product, currentSlide);
                }
            });
        } else {//mobile
            var $sliderCatalog = $('.products-slider.is-catalog'),
                sliderScrollTimer;

            ecommerceCatalogShow($sliderCatalog);
            $(document).on('scroll', function() {
                sliderScrollTimer && clearTimeout(sliderScrollTimer);
                sliderScrollTimer = setTimeout(function() {
                    ecommerceCatalogShow($sliderCatalog);
                }, 250);
            });
        }
    }
    //Мы рекомендуем на главной

    //Каталог
    var $catalog = $('.products-list'),
        catalogScrollTimer;

    ecommerceCatalogShow($catalog);
    $(document).on('scroll', function() {
        catalogScrollTimer && clearTimeout(catalogScrollTimer);
        catalogScrollTimer = setTimeout(function() {
            ecommerceCatalogShow($catalog);
        }, 250);
    });
    //Каталог

};

document.addEventListener("DOMContentLoaded", ecommerceInit);