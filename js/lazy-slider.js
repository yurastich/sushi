'use strict';

(function ($) {

    var methods = {

        init : function(options) {
            return this.each(function() {
                var settings = $.extend({
                    size : {
                        lg : '1920',
                        md : '1440',
                        sm : '1024',
                        xs : '768',
                        mobile : '420'
                    }
                }, options);
                var $self = $(this),
                    mobile = $self.lazySlider('isMobile'),
                    resizeTimer,
                    resizeStart;

                $self.data('settings', settings);
                $self.lazySlider('lazy');

                if ( mobile ) {
                    $(window).on('orientationchange', function() {
                        setTimeout(function() {
                            $self.lazySlider('lazy');
                        }, 500);
                    });
                } else {
                    $(window).on('resize', function() {
                        if ( !resizeStart ) {
                            $self.lazySlider('showLoader');
                            resizeStart = true;
                        }
                        clearTimeout(resizeTimer);
                        resizeTimer = setTimeout(function() {
                            $self.lazySlider('lazy');
                            resizeStart = false;
                        }, 250);
                    });
                }


            });
        },

        isMobile : function () {
            var useragents = ['android','astel','audiovox','blackberry','chtml','docomo','ericsson','hand','iphone ','ipod','2me','ava','j-phone','kddi','lg','midp','mini','minimo','mobi','mobile','mobileexplorer','mot-e','motorola','mot-v','netfront','nokia', 'palm','palmos','palmsource','pda','pdxgw','phone','plucker','portable','portalmmm','sagem','samsung','sanyo','sgh','sharp','sie-m','sie-s','smartphone','softbank','sprint','symbian','telit','tsm','vodafone','wap','windowsce','wml','xiino'];

            var agt = navigator.userAgent.toLowerCase(),
                mobile = false;

            for(var i = 0; i < useragents.length; i++) {
                if( agt.indexOf(useragents[i])!=-1 ) {
                    mobile = true;
                    break;
                }
            }

            return mobile;
        },

        showLoader : function () {
            var $this = $(this);
            $this.addClass('lazy-slider-loader');
        },

        hideLoader : function () {
            var $this = $(this);
            $this.removeClass('lazy-slider-loader');
        },

        lazy : function () {
            var $this = $(this),
                settings = $this.data('settings'),
                $images = $this.find('.lazy-slider-img'),
                srcType;

            $this.lazySlider('showLoader');

            for (var key in settings.size) {
                if ($(window).width() <= settings.size[key]) {
                    srcType = key;
                }
            }

            $images.each(function (index, element) {
                var $element = $(element),
                    src = $element.data(srcType + '-src');
                $element.attr('src', src);

            });

            $this.lazySlider('hideLoader');
        }
    };

    $.fn.lazySlider = function (method) {
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метод с именем ' +  method + ' не существует для jQuery.lazySlider' );
        }
    };

})(jQuery);