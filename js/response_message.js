var ResponseMessage = {

    popup : 'response-popup',

    loadModal : function (target) {
        var $modal = jQuery(target);

        if ( $modal.length && $modal.text().length ) {
            this.show( '', $modal.text() );
        }
    },

    show : function (title, message) {
        var $popup = jQuery('#' + ResponseMessage.popup),
            $popup_title = $popup.find('[data-response-title]'),
            $popup_message = $popup.find('[data-response-message]');

        if ($popup.hasClass('active')) {
            return false;
        }

        if (title) {
            $popup_title.html(title);
        } else {
            $popup_title.empty();
        }
        if (message) {
            $popup_message.html(message);
        } else {
            $popup_message.empty();
        }

        Popup.hideAll();
        Popup.show(ResponseMessage.popup);
    },

    hide : function () {
        Popup.hide(ResponseMessage.popup);
        jQuery(ResponseMessage.popup).find('[data-response-title]').empty();
        jQuery(ResponseMessage.popup).find('[data-response-message]').empty();
    }

};

jQuery(document).ready(function () {
    ResponseMessage.loadModal('#modal-promo-message');
});

