var useragents = ['android', 'astel', 'audiovox', 'blackberry', 'chtml', 'docomo', 'ericsson', 'hand', 'iphone ', 'ipod', '2me', 'ava', 'j-phone', 'kddi', 'lg', 'midp', 'mini', 'minimo', 'mobi', 'mobile', 'mobileexplorer', 'mot-e', 'motorola', 'mot-v', 'netfront', 'nokia', 'palm', 'palmos', 'palmsource', 'pda', 'pdxgw', 'phone', 'plucker', 'portable', 'portalmmm', 'sagem', 'samsung', 'sanyo', 'sgh', 'sharp', 'sie-m', 'sie-s', 'smartphone', 'softbank', 'sprint', 'symbian', 'telit', 'tsm', 'vodafone', 'wap', 'windowsce', 'wml', 'xiino'];

var agt = navigator.userAgent.toLowerCase();
var is_mobile = false;

function supports_html5_storage(){
    try {
        return 'localStorage' in window && window['localStorage'] !== null;
    } catch(e) {
        return false;
    }
};

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

for (var i = 0; i < useragents.length; i++) {
    if (agt.indexOf(useragents[i]) != -1) {
        is_mobile = true;
        user_agent = agt;
        break;
    }
}

if (is_mobile) {
    document.getElementsByTagName('html')[0].classList.add('is-mobile');
}

var pageMask = {
    show : function () {
        $('body').addClass('page-disabled');
    },

    hide : function () {
        $('body').removeClass('page-disabled');
    }
};

function setPageTop() {
    var $header = $('header'),
        $menu = $('#category-menu'),
        height = $header.height();

    if ( $('#fake-div').is(':hidden') && $menu.length ) {
        height += $menu.height();
    }

    $('main').css('margin-top', height + 'px');
};

var ScrollTable = {

    init: function() {
        var $all = $('article table');
        if ( $all.length ) {
            ScrollTable.replace($all);
        }
    },

    replace: function($all) {
        //clear in article table inline style
        $all.each(function(index, element) {
            var $this = jQuery(element);
            $this.removeAttr('style').removeAttr('border').removeAttr('cellspacing').removeAttr('cellpadding');
            $this.find('thead, tbody, tfooter, tr, td, th, p').removeAttr('style').removeAttr('border').removeAttr('cellspacing').removeAttr('cellpadding').removeAttr('rel');
            $this.wrap("<div class='table-container'></div>");
        });
    }

};

var MainSlider = {
    init : function ($slider, settings, position) {

        if ( !$slider.length || $slider.hasClass('slick-initialized') ) {
            return true;
        }

        $slider.slick({
            dots: false,
            autoplay : true,
            autoplaySpeed: $slider.attr('data-autoplaySpeed') || 2000,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1
        });
        $slider.closest('.lazy-slider-section').lazySlider(settings);
        $slider.on('afterChange', function(event, slick, currentSlide, nextSlide){
            ecommerceSliderShow($slider, position);
        });

    }
};

function isSupportStorage(success, error) {
    if (typeof localStorage === 'object') {
        try {
            localStorage.setItem('localStorage', 1);
            localStorage.removeItem('localStorage');
            success();
        } catch (e) {
            error();
        }
    }
};

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
};

function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
};

function deleteCookie(name) {
    setCookie(name, "", {
        expires: -1
    })
};

function initCountDown(element) {
    if ( !$(element).length ) {
        return false;
    }
    $(element).each(function(index, item) {
        var $this = $(item),
            daysTitle = $this.data('daysTitle') || '',
            hoursTitle = $this.data('hoursTitle') || '',
            minutesTitle = $this.data('minutesTitle') || '',
            secondsTitle = $this.data('secondsTitle') || '',
            delimiter = $this.data('delimiter') || '';

        $this.countdown($this.data('countdown'));

        $this.on('update.countdown', function(event) {
            var format = '';
            if (event.offset.totalDays) {
                format += '<div class="countdown-column is-days"><span class="countdown-title">'+daysTitle+'</span><span class="countdown-days">%-D</span></div><div class="countdown-delimiter is-days">'+delimiter+'</div>';
            }
            if (event.offset.hours) {
                format += '<div class="countdown-column is-hours"><span class="countdown-title">'+hoursTitle+'</span><span class="countdown-hours">%-H</span></div><div class="countdown-delimiter is-hours">'+delimiter+'</div>';
            }
            format += '<div class="countdown-column is-minutes"><span class="countdown-title">'+minutesTitle+'</span><span class="countdown-minutes">%M</span></div><div class="countdown-delimiter is-minutes">'+delimiter+'</div>';
            format += '<div class="countdown-column is-seconds"><span class="countdown-title">'+secondsTitle+'</span><span class="countdown-seconds">%S</span></div>';
            $this.html(event.strftime(format));
        });
    });
};

window.onload = function () {
    $('#preloader').fadeOut(300);
};

$(document).ready(function () {

    isSupportStorage(function () {

    }, function () {
        if ( !getCookie('showSupportStorageMessage') ) {
            ResponseMessage.loadModal('#modal-support-storage-message');

            var dateExp = $('#modal-support-storage-message').data('expires') || 10,
                date = new Date(new Date().getTime() + (dateExp * 60000) );
            setCookie('showSupportStorageMessage', 1, {
                path: '/',
                expires: date.toUTCString()
            });
        }
    });

    setPageTop();

    MainSlider.init($('#lazy-slider-section .lazy-slider'), null, 'slider-top-1');
    MainSlider.init($('#lazy-banner-section .lazy-slider'), null, 'slider-bottom-1');
    ScrollTable.init();

    if ( is_mobile ) {
        $(window).on('orientationchange', function () {
            setTimeout(function() {
                setPageTop();
            }, 500);
        });
    } else {
        $(window).on('resize', function () {
            setTimeout(function() {
                setPageTop();
            }, 500);
        });
    }

    $('html').on('keypress', '[data-number-input]', function (e) {
        //0123456789+-
        var key = e.keyCode || e.which;
        if ( key >= 37 && key <= 57 || key == 8) {
        } else {
            return false;
        }
    });

    $('html').on('keypress', '[data-numeric-input]', function (e) {
        //0123456789
        var key = e.keyCode || e.which;
        if ( (key >= 37 && key <= 40) || (key >= 48 && key <= 57) || key == 13 ) {
        } else {
            return false;
        }
    });

    $(".header-inner .detected-city-popup .close").click(function (e) {
        e.preventDefault();
        var self = $(this);
        jQuery.ajax({
            url:        $(this).data('url'),
            type:       'GET',
            dataType:   'json',
            cache:      false,
            success:    function(response) {
                self.parent().hide();
            },
            error:    function(response) {
                self.parent().hide();
            }
        });
    });
    $(".header-inner .detected-city-popup .change-city").click(function (e) {
        e.preventDefault();
        $(this).parent().hide();
        $(".header-inner .cities-list-popup").toggle();

        if( $(".header-inner .cities-list-popup").is( ":visible" ) ){
            $(document).bind('mouseup touchend', mouseUpHandlerCity );
        }else{
            $(document).unbind('mouseup touchend', mouseUpHandlerCity );
        }
    });
    $(".header-inner .detected-city-popup .btn-yes").click(function (e) {
        e.preventDefault();
        var $self = $(this);
        return $.ajax({
            'cache': false,
            method: "GET",
            url: $(this).attr('href'),
        }).done(function () {
            $self.parent().hide();
        }).fail(function (response) {
        });
    });
    $(".header-inner .city-toggle").click(function (e) {
        e.preventDefault();
        $(".header-inner .detected-city-popup").hide();
        $(".header-inner .cities-list-popup").toggle();

        var $list_overflow = $('.header-inner .city .list-popup-scroll');

        if( $(".header-inner .cities-list-popup").is( ":visible" ) ){
            $(document).bind('mouseup touchend', mouseUpHandlerCity );
            if ($list_overflow.length) {
                $list_overflow.perfectScrollbar();
            }
        }else{
            $(document).unbind('mouseup touchend', mouseUpHandlerCity );
            if ($list_overflow.length && $list_overflow.hasClass('ps-container') ) {
                $list_overflow.perfectScrollbar('destroy');
            }
        }

    });
    function mouseUpHandlerCity (e)
    {
        var  container = $(".city"),
            container2 = $(".city *"),
            $list_overflow = $('.header-inner .city .list-popup-scroll');


        if (!container.is(e.target) &&  !container2.is(e.target) ){ // ... nor a descendant of the container

            $(document).unbind('mouseup touchend', mouseUpHandlerCity); //Remove the event listener to prevent memory leaks
            $(".header-inner .cities-list-popup").hide();
            if ($list_overflow.length && $list_overflow.hasClass('ps-container') ) {
                $list_overflow.perfectScrollbar('destroy');
            }
        }

    };

    $(".header-inner .restaurant-toggle").click(function (e) {
        e.preventDefault();
        $(".header-inner .restaurants-list-popup").toggle();

        var $list_overflow = $('.header-inner .restaurant .list-popup-scroll');

        if( $(".header-inner .restaurants-list-popup").is( ":visible" ) ){
            $(document).bind('mouseup touchend', mouseUpHandlerRestaurant );
            if ($list_overflow.length) {
                $list_overflow.perfectScrollbar();
            }
        }else{
            $(document).unbind('mouseup touchend', mouseUpHandlerRestaurant );
            if ($list_overflow.length && $list_overflow.hasClass('ps-container') ) {
                $list_overflow.perfectScrollbar('destroy');
            }
        }
    });

    function mouseUpHandlerRestaurant (e)
    {
        var  container = $(".restaurant"),
            container2 = $(".restaurant *"),
            $list_overflow = $('.header-inner .restaurant .list-popup-scroll');

        if (!container.is(e.target) &&  !container2.is(e.target) ){ // ... nor a descendant of the container

            $(document).unbind('mouseup touchend', mouseUpHandlerRestaurant); //Remove the event listener to prevent memory leaks
            $(".header-inner .restaurants-list-popup").hide();
            if ($list_overflow.length && $list_overflow.hasClass('ps-container') ) {
                $list_overflow.perfectScrollbar('destroy');
            }

        }

    };

    $(".js-header-wrapper .langs-toggle").click(function (e) {
        e.preventDefault();

        var $this = $(this),
            $container = $this.closest('.js-header-wrapper'),
            $popup = $container.find('.langs-list-popup');

        $popup.toggle();

        if( $popup.is( ":visible" ) ){
            $(document).bind('mouseup touchend', mouseUpHandlerLangs );
        }else{
            $(document).unbind('mouseup touchend', mouseUpHandlerLangs );
        }
    });

    function mouseUpHandlerLangs (e)
    {
        if ( !is_mobile ) {
            var  container = $(".langs"),
                container2 = $(".langs *");

            if (!container.is(e.target) &&  !container2.is(e.target) ){ // ... nor a descendant of the container

                $(document).unbind('mouseup touchend', mouseUpHandlerLangs); //Remove the event listener to prevent memory leaks
                $(".js-header-wrapper .langs-list-popup").hide();
            }
        }

    };


    $(".header-inner .delivery-toggle").click(function (e) {
        e.preventDefault();
        $(".header-inner .delivery-list-popup").toggle();

        if( $(".header-inner .delivery-list-popup").is( ":visible" ) ){
            $(document).bind('mouseup touchend', mouseUpHandlerDelivery );
        }else{
            $(document).unbind('mouseup touchend', mouseUpHandlerDelivery );
        }
    });

    function mouseUpHandlerDelivery (e)
    {
        var  container = $(".delivery"),
            container2 = $(".delivery-toggle"),
            container3 = $(".delivery-list-popup"),
            container4 = $(".delivery-list-popup *");

        if (!container.is(e.target) &&  !container2.is(e.target) &&  !container3.is(e.target) &&  !container4.is(e.target) ){ // ... nor a descendant of the container

            $(document).unbind('mouseup touchend', mouseUpHandlerDelivery); //Remove the event listener to prevent memory leaks
            $(".header-inner .delivery-list-popup").hide();
        }

    };

    $(".header-inner").on('click', '.cart-toggle', function (e) {
        e.preventDefault();

        if($(this).hasClass('empty')){
            return;
        }

        $("#popup-cart").toggle();


        if( $("#popup-cart").is( ":visible" ) ){
            $("html").addClass("popup-cart-open");
            $("#popup-cart").addClass("open");
            $(document).bind('mouseup touchend', mouseUpHandlerCart);
            $('#popup-cart .cart-item-list').perfectScrollbar('update');
        }else{
            $("html").removeClass("popup-cart-open");
            $("#popup-cart").removeClass("open");
            $(document).unbind('mouseup touchend', mouseUpHandlerCart );
        }
    });

    function mouseUpHandlerCart (e)
    {
        var  container = $(".cart-container"),
            container2 = $(".cart-container *"),
            container3 = $(".cart *");


        if (!container.is(e.target) &&  !container2.is(e.target) &&  !container3.is(e.target)){ // ... nor a descendant of the container

            $(document).unbind('mouseup touchend', mouseUpHandlerCart); //Remove the event listener to prevent memory leaks
            $("html").removeClass("popup-cart-open");
            $("#popup-cart").removeClass("open");
            $("#popup-cart").toggle();

        }

    }

    function closeCart() {
        $(".cart .cart-toggle").addClass('empty')
        $(document).unbind('mouseup touchend', mouseUpHandlerCart); //Remove the event listener to prevent memory leaks
        $("#popup-cart").removeClass("open");
        $("#popup-cart").toggle();
    }

    $(document).on('cart.empty', closeCart);

    $(".restaurants-filter .city-toggle").click(function (e) {
        e.preventDefault();

        var $list_overflow = $('.restaurants-filter .cities-list-overflow');

        $(".restaurants-filter .cities-list-popup").toggle();

        if( $(".restaurants-filter .cities-list-popup").is( ":visible" ) ){
            $(document).bind('mouseup touchend', mouseUpHandlerCities );

            if ( $list_overflow.hasClass('ps-container') ) {
                $list_overflow.perfectScrollbar('update');
            } else {
                $list_overflow.perfectScrollbar();
            }

        }else{
            $(document).unbind('mouseup touchend', mouseUpHandlerCities );
        }
    });

    function mouseUpHandlerCities (e)
    {
        if ( is_mobile ) {
            return true;
        }
        var  container = $(".city"),
            container2 = $(".city-toggle"),
            container3 = $(".cities-list-popup");

        if (!container.is(e.target) &&  !container2.is(e.target) &&  !container3.is(e.target) ){ // ... nor a descendant of the container

            $(document).unbind('mouseup touchend', mouseUpHandlerCities); //Remove the event listener to prevent memory leaks
            $(".restaurants-filter .cities-list-popup").hide();
        }

    }

    $('body').on('click','.restaurants-filter .street-toggle',function(e){

        var $list_overflow = $('.restaurants-filter .streets-list-overflow');

        $(".restaurants-filter .streets-list-popup").toggle();

        if( $(".restaurants-filter .streets-list-popup").is( ":visible" ) ){
            $(document).bind('mouseup touchend', mouseUpHandlerStreets );

            if ( $list_overflow.hasClass('ps-container') ) {
                $list_overflow.perfectScrollbar('update');
            } else {
                $list_overflow.perfectScrollbar();
            }

        }else{
            $(document).unbind('mouseup touchend', mouseUpHandlerStreets );
        }
    });

    function mouseUpHandlerStreets (e)
    {
        var  container = $(".street"),
            container2 = $(".street-toggle"),
            container3 = $(".streets-list-popup");

        if (!container.is(e.target) &&  !container2.is(e.target) &&  !container3.is(e.target) ){ // ... nor a descendant of the container

            $(document).unbind('mouseup touchend', mouseUpHandlerStreets); //Remove the event listener to prevent memory leaks
             $(".restaurants-filter .streets-list-popup").hide();
        }

    }

    $(".sales-filter .city-toggle").click(function (e) {
        e.preventDefault();
        $(".sales-filter .cities-list-popup").toggle();

        if( $(".sales-filter .cities-list-popup").is( ":visible" ) ){
            $(document).bind('mouseup touchend', mouseUpHandlerSalesCities );
        }else{
            $(document).unbind('mouseup touchend', mouseUpHandlerSalesCities );
        }
    });

    function mouseUpHandlerSalesCities (e)
    {
        var  container = $(".city"),
            container2 = $(".city-toggle"),
            container3 = $(".cities-list-popup");

        if (!container.is(e.target) &&  !container2.is(e.target) &&  !container3.is(e.target) ){ // ... nor a descendant of the container

            $(document).unbind('mouseup touchend', mouseUpHandlerSalesCities); //Remove the event listener to prevent memory leaks
            $(".sales-filter .cities-list-popup").hide();
        }

    }

    $(".sales-filter .type-toggle").click(function (e) {
        e.preventDefault();
        $(".sales-filter .types-list-popup").toggle();

        if( $(".sales-filter .types-list-popup").is( ":visible" ) ){
            $(document).bind('mouseup touchend', mouseUpHandlerSalesTypes );
        }else{
            $(document).unbind('mouseup touchend', mouseUpHandlerSalesTypes );
        }
    });

    function mouseUpHandlerSalesTypes (e)
    {
        var  container = $(".type"),
            container2 = $(".type-toggle"),
            container3 = $(".types-list-popup");

        if (!container.is(e.target) &&  !container2.is(e.target) &&  !container3.is(e.target) ){ // ... nor a descendant of the container

            $(document).unbind('mouseup touchend', mouseUpHandlerSalesTypes); //Remove the event listener to prevent memory leaks
            $(".sales-filter .types-list-popup").hide();
        }

    }


    $(".type-filter .filter-toggle").click(function (e) {
        e.preventDefault();
        $(".type-filter .filter-list-popup").toggle();

        if( $(".type-filter .filter-list-popup").is( ":visible" ) ){
            $(document).bind('mouseup touchend', mouseUpHandlerFilterTypes );
        }else{
            $(document).unbind('mouseup touchend', mouseUpHandlerFilterTypes );
        }
    });

    function mouseUpHandlerFilterTypes (e)
    {
        var  container = $(".type-filter *");

        if (!container.is(e.target) ){ // ... nor a descendant of the container

            $(document).unbind('mouseup touchend', mouseUpHandlerFilterTypes); //Remove the event listener to prevent memory leaks
            $(".type-filter .filter-list-popup").hide();
        }

    }

    $(".sort-filter .sort-filter-toggle").click(function (e) {
        e.preventDefault();
        $(".sort-filter .sort-filter-list-popup").toggle();

        if( $(".sort-filter .sort-filter-list-popup").is( ":visible" ) ){
            $(document).bind('mouseup touchend', mouseUpHandlerFilterSort );
        }else{
            $(document).unbind('mouseup touchend', mouseUpHandlerFilterSort );
        }
    });

    function mouseUpHandlerFilterSort (e)
    {
        var  container = $(".sort-filter *");

        if (!container.is(e.target)  ){ // ... nor a descendant of the container

            $(document).unbind('mouseup touchend', mouseUpHandlerFilterSort); //Remove the event listener to prevent memory leaks
            $(".sort-filter .sort-filter-list-popup").hide();
        }

    }

    /*cart scrollbar*/
        $('#popup-cart .cart-item-list').perfectScrollbar();

        if ( !is_mobile ) {
            $('.full-menu').perfectScrollbar();
        }

    /*cart scrollbar*/

    $(".restaurant-slider-section .restaurant-slider").slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1
    });

    $(".delivery-slider").slick({
        dots: false,
        arrows: true,
        infinite: true,
        speed: 300,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 780,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    if( $('.products-slider').length ) {
        if ( $(window).width() >= 766 ) {
            $(".products-slider").removeClass('is-catalog').slick({
                dots: false,
                arrows: true,
                infinite: true,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 3000,
                responsive: [
                    {
                        breakpoint: 1600,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 640,
                        settings: {
                            slidesToShow: 1
                        }
                    }
                ]
            });
        } else {
            $(".products-slider").addClass('is-catalog');
        }
    }

    $(".free-gift").click(function (e) {
        e.preventDefault();
        $(this).find(".gifts-list").toggle();
    });

    $(".gift-to-choose").click(function (e) {
        e.preventDefault();
        var parent = $(this).closest(".free-gift");
        var name = parent.find(".user-gift").find(".name");
        name.text($(this).data('name'));
        name.attr('data-product-id', $(this).attr('data-product-id'));
    });

    //hide gift select
    $('html').on('click', function (e) {
        if ( !$(e.target).closest('.free-gift').length ) {
            $(".gifts-list").hide();
        }
    });

    $("header .main-menu").click(function(e){
        e.preventDefault();

            $(this).toggleClass("open");
            $('html').toggleClass('full-menu-open');
            $(".full-menu").toggleClass("open");
            $(".full-menu .full-main-menu > li").removeClass('hover');
            $(".full-menu .full-main-menu li:first-child").addClass('hover');

            if ($(".full-menu").hasClass("open")) {
                $(document).bind('mouseup touchend', mouseUpHandlerMenu);
            } else {
                $(document).unbind('mouseup touchend', mouseUpHandlerMenu);
            }

    });

    function mouseUpHandlerMenu (e)
    {
        var  container = $(".full-menu"),
            container2 = $(".full-menu *"),
            container3 = $("header .main-menu *");

        if (!container.is(e.target) && !container2.is(e.target) && !container3.is(e.target) ){ // ... nor a descendant of the container

            $(document).unbind('mouseup touchend', mouseUpHandlerMenu); //Remove the event listener to prevent memory leaks

            $("header .main-menu").removeClass("open");
            $('html').removeClass('full-menu-open');
            $(".full-menu").removeClass("open");
        }

    };

    $( ".full-menu .full-main-menu > li" )
        .mouseenter(function() {
            if($(document).width() > 767){
                $(".full-menu .full-main-menu > li").removeClass('hover');
                $(this).toggleClass('hover');
            }
        })
        .mouseleave(function() {
            if($(document).width() > 767) {
                /*$(this).toggleClass('hover');*/
            }
        });

    $( ".full-menu .full-main-menu > li" ).click(function() {
        if($(document).width() < 767){
            if(!$(this).hasClass('hover')) {
                $(".full-menu .full-main-menu > li").removeClass('hover');
                $(this).toggleClass('hover');
            }else{
                $(this).toggleClass('hover');
            }
        }
    });

    //$('.fast-categories ul').smoothTouchScroll();

    $(" .tab").click(function(){
        $(".tab").removeClass("active");
        $(this).addClass("active");
        $("div[data-tab]").addClass("hidden");
        var _tab = $(this).data("tabs");
        $("div[data-tab='"+_tab+"']").removeClass("hidden");
    });


    $(".count-field").on("keypress change",function(e){
        // allow numbers only.
        var charCode = e.which || e.keyCode;
        if(charCode == 37 || charCode == 39) {

        }
        else if(!(charCode >= 48 && charCode <= 57) && charCode != 9 && charCode != 8) {
            e.preventDefault();
        }
    });


    /*profile history*/
    $(".history-container .history-item .title").click(function(e){
        e.preventDefault();
        $(this).parent().toggleClass("open");
    });
    /*profile history*/


    /*business-lunch*/


    $(".dish-to-choose input[type=checkbox]").click(function(){

        var name = $(this).prop('name');

        if($(this).attr('previousValue') == 'true'){
            $('.dish-to-choose input[name=' + name + ']').attr('previousValue', false);
            $('.dish-to-choose input[name=' + name + ']').prop('checked', false);
            $(this).prop('checked', false);
            $(this).attr('previousValue', false);
        } else {
            $('.dish-to-choose input[name=' + name + ']').attr('previousValue', false);
            $('.dish-to-choose input[name=' + name + ']').prop('checked', false);
            $(this).prop('checked', true);
            $(this).attr('previousValue', true);
        }

        checkDishes();

    });

    function checkDishes () {
        var $price_block = $(".business-lunch-list .price-block"),
            $counter = $price_block.find('.buy_business'),
            $counter_input = $counter.find('.count-field'),
            n = $(".dish-to-choose input:checked").length;

        $price_block.hide();

        //reset counter
        $counter.removeClass('active');
        $counter_input.val('');

        if (n == 2) {
            $("#two-dishes").show();
            $("#two-dishes2").show();
        } else if (n == 3) {
            $("#three-dishes").show();
            $("#three-dishes2").show();
        }
    }

    /*business-lunch*/

    /*masks*/
    $(function() {
        $('input.phone').mask('+38(000) 000-00-00', {clearIfNotMatch: true});
    });
    /*masks*/
    
    /*scroll header*/
    var lastScrollTop = 0,
        headerIsFixed = false;

    $(window).scroll(function () {

        if ( $(document).width() >= 1024 ) {
            if ( $('#category-menu').attr('style') ) {
                $('#category-menu').css('top', '');
            }

            if ( $('header').attr('style') ) {
                $('header').css('top', '');
            }

            return true;
        }

        if ( $('html').hasClass('full-menu-open') ) {
            return true;
        }

        var scrollTop = $(window).scrollTop();

        if ( scrollTop > 60 && scrollTop > lastScrollTop ) {
            if ( !headerIsFixed ) {
                $('#category-menu').css('top', '0');
                $('header').css('top', '-60px');
                headerIsFixed = true;
            }
        } else {
            if ( headerIsFixed ) {
                $('#category-menu').css('top', '60px');
                $('header').css('top', '0');
                headerIsFixed = false;
            }
        }

        lastScrollTop = scrollTop;
    });
    /*scroll header*/

    /*validation*/

        // validate signup form on keyup and submit
        $('form[name="reviews"]').validate({
            rules: {
                message: "required",
                name: "required",
                type: "required",
                phone: "required",
                email: {
                    required: true,
                    email: true
                },
                restaurant: "required"
            },
            messages: {
                message: "Please enter your message",
                name: "Please enter your message",
                type: "Please enter your message",
                email: "Please enter a valid email address",
                phone: "Please accept our policy"
            },
            submitHandler: function(form) {
                var $form = $(form);
                var city = $('div.city').find('a.city-toggle').find("span").html();
               if($form.find('input[name="city"]').length<1) $form.append('<input type="hidden" name="city" value="'+city+'">');
                $('.loader_feedback').show();

                $.post("/review-add", {data : $('form[name="reviews"]').serialize() },
                    function(data){
                        if (data.status == 'success') {
                            $form.find('.errors, .form-response').hide();
                            $form.hide();
                            $form.siblings('.form-response').html( data.message ).show(200);

                        } else if (data.status == 'error') {
                            $('form[name="reviews"] .errors').show().html(data.message);
                        }
                        $('.loader_feedback').hide();
                }, 'json');

            }
        });

    /*validation*/



    initConstructorCarousel();
    initConstructorWideSlider();


    });

function initConstructorCarousel () {

    $(".products-slider").each(function(){
        if (!$(this).hasClass('slick-initialized') && $(this).find('[data-vis-role="slide"]').length) {
           $(this).slick({
                dots: true,
                arrows: false,
                infinite: true,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 1600,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 640,
                        settings: {
                            slidesToShow: 1
                        }
                    }
                ]
            });
        }
    });
}

function initConstructorWideSlider () {
    $("[data-js-construktor-slider-section] .lazy-slider").each(function(){
        if ( $(this).find('[data-vis-role="slide"]').length ) {
            MainSlider.init($(this), null, 'slider-constructor');
        }
    });
};

function getUrlLangPrefix() {
    if ($('html').attr('lang') && $('html').attr('lang') != 'ru') {
        return '/' + $('html').attr('lang');
    }
    return '';
};
