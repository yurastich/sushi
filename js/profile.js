"use strict";

var Profile =
    {
        lookup_timer: false,
        lookup_text: false,
        lookup_results: {},
        personal_data_form: '#personal-data-form',
        personal_address_form: '#add-new-address-form',

        init: function () {
            this.validatePersonalDataForm();
            this.validatePersonalAddressForm();
            this.events();
        },

        triggerPersonalDataForm: function () {
            $(this.personal_data_form).submit();
        },

        validatePersonalDataForm: function() {

            if ( !$(this.personal_data_form).length ) {
                return false;
            }

            $(this.personal_data_form).find('input[name="user_pass"]').fileUpload({
                maxFileSize : 22, //mb
                onError: function (err) {
                    if (Object.keys(err).length && err.type && err.type === 'maxFileSize') {
                        var msg = $('#js-user-pass-max-file-size-msg').text();
                        msg.length && alert(msg);
                    }
                }
            });

            var currentDate = new Date();

            $(this.personal_data_form).validate({
                rules: {
                    name: "required",
                    email: {
                        required: true,
                        email: true
                    },
                    'birthday[day]' : {
                        required: true,
                        digits: true,
                        min: 1,
                        max: 31
                    },
                    'birthday[month]' : {
                        required: true,
                        digits: true,
                        min: 1,
                        max: 12
                    },
                    'birthday[year]' : {
                        required: true,
                        digits: true,
                        min: currentDate.getFullYear() - 150,
                        max: currentDate.getFullYear() - 3
                    }
                },
                errorPlacement : function(error, element) {},
                submitHandler: function(form) {
                    form.submit();
                }
            });

        },

        validatePersonalAddressForm: function() {

            if ( !$(this.personal_address_form).length ) {
                return false;
            }

            $(this.personal_address_form).validate({
                rules: {
                    'address[street_name]': "required",
                    'address[house]': "required"
                },
                errorPlacement : function(error, element) {},
                submitHandler: function(form) {
                    form.submit();
                }
            });

        },

        lookup: function(){
            var idCity = $("#add-new-address-form select[name='address[city_name]']").find(':selected').data('id');
            if (typeof idCity === 'undefined' || typeof Profile.lookup_text === 'undefined') {
                return true;
            }

            $.ajax({
                url: 'api/cities/' + idCity + '/streets',
                type: 'get',
                async: true,
                data: {'q': Profile.lookup_text},
                dataType: 'json',
                success: function(data) {
                    Profile.parseLookup(data);
                },
                error: function (xhr, status, error) {
                    console.log('Some errors: ' + status + error);
                }
            });
        },
        initLookup: function(string) {
            string = string.replace(/[\s]{2,}/gi, ' ');
            string = string.trim();
            $('#address_submit_btn').prop("disabled", true);
            var lookup_results;

            if (!string) {
                $('.fast_result').hide();
                return false;
            }

            if (string !== this.lookup_text) {
                clearTimeout(this.lookup_timer);
                Profile.lookup_text = string;
                this.lookup_timer = setTimeout(Profile.lookup, 250);
            }
        },
        parseLookup: function(data) {
            if (data.errors || typeof data.data.streets === 'undefined') return false;

            var searchResults = [],
                i = 0;

            $.each(data.data.streets, function(index, street) {
                if (i > 8) return true;
                searchResults.push(street.name);
                i++;
            });

            $("#add-new-address-form input[name='address[street_name]']").autocomplete({
                source: searchResults,
                select: function (e, ui) {
                    $('#address_submit_btn').prop('disabled', false);
                    //$('#address_submit_btn').removeAttr('disabled').off('click');
                }
            });
        },

        removeAddress: function (self, index) {
            if ( confirm( self.getAttribute('data-confirm-message') ) ) {
                var label = "address" + index;
                $('label[for=' + label +']').remove();


                var form = $('#address-list');
                $.ajax({
                    type: "POST",
                    url: '/profile',
                    data: form.serialize(),
                    success: function(response) {
                        console.log(response);
                    }
                });
            }

            return true;
        },

        events : function () {

            $("#add-new-address-form input[name='address[street_name]']").keyup(function (e) {
                if (e.keyCode == 13 ) {
                    return true;
                }
                Profile.initLookup(this.value);
            });

        }
    };


var Profile618 = {

    page : '.profile-page',
    step1Form : '#618-sms-phone-form',
    step2Form : '#618-sms-code-form',

    init : function () {

        this.initCodeCountDown();
        this.events();
    },

    initCodeCountDown : function () {
        if ( $('#js-profile-code-countdown').length ) {
            var totalSecs = parseInt($('#js-profile-code-countdown').data('time')),
                d = (typeof window['loadPageTime'] !== 'undefined') ? window['loadPageTime'] : new Date(),
                year, month, date, hours, minutes, seconds, str;

            d.setSeconds(totalSecs);

            year = d.getFullYear();
            month = d.getMonth() + 1;
            date = (d.getDate() < 10) ? '0' + d.getDate() : d.getDate();
            hours = (d.getHours() + 1 < 10) ? '0' + d.getHours() : d.getHours();
            minutes = (d.getMinutes() < 10) ? '0' + d.getMinutes() : d.getMinutes();
            seconds = (d.getSeconds() < 10) ? '0' + d.getSeconds() : d.getSeconds();
            str = year + '/' + month + '/' + date + ' ' + hours + ':' + minutes + ':' + seconds;

            $('#js-profile-code-countdown').data('countdown', str);
            initCountDown('#js-profile-code-countdown');
        }
    },

    setStep : function (step) {
        var $elements = $('.profile-container-section'),
            $current = $elements.filter('[data-step="' + step + '"]');

        $elements.not($current).removeClass('active');
        $current.addClass('active');
    },

    validateStep1 : function () {
        var self = this,
            $form = $(self.step1Form),
            phone = $form.find('input[name="phone"]').val().replace(/[^\d]/g,''),
            error_message = $form.find('.js-error-message').text();

        if ( !phone.length ) {
            $.notify(error_message, 'error');
            return false;
        }

        return true;
    },

    sendStep1 : function () {
        var self = this,
            $page = $(self.page),
            $form = $(self.step1Form),
            phone = $form.find('input[name="phone"]').val().replace(/[^\d]/g,''),
            success_message = $form.find('.js-success-message').text(),
            recaptcha = $form.find('[name="g-recaptcha-response"]').val();

        if (typeof window['login_recaptcha'] !== 'undefined') {
            grecaptcha.reset( window['login_recaptcha'] );
        }

        $page.addClass('disabled-mask');
        $.ajax({
            'cache': false,
            method: "POST",
            url: '/api/sessions/' + phone,
            data: {recaptcha: recaptcha}
        }).done(function (response) {
            $page.removeClass('disabled-mask');
            if (typeof response.data.user !== 'undefined') {
                window.location.href = '/618/thanks';
                // location.reload();
            } else {
                $.notify(success_message, 'success');
                if (typeof response.data.register_form !== 'undefined' && response.data.register_form) {
                    $(self.step2Form).append(response.data.register_form);
                }
                self.setStep(2);
            }
        }).fail(function (response) {
            $page.removeClass('disabled-mask');
            response.responseJSON.errors.forEach(function (error) {
                $.notify(error, 'error');
            });
        });
    },

    validateStep2 : function () {
        var self = this,
            $page = $(self.page),
            $form = $(self.step2Form),
            phone = $(self.step1Form).find('input[name="phone"]').val().replace(/[^\d]/g,''),
            // register = $form.find('input[name="confirm"]').val(),
            error_message = $form.find('.js-error-message').text(),
            code = $form.find('input[name="code"]'),
            register = $("input[name=register]").val();

        if (!code.val().length) {
            $.notify(error_message, 'error');
            return;
        }

        $page.addClass('disabled-mask');

        if ( !phone.length ) {
            $.notify(error_message, 'error');
            return false;
        }

        return $.ajax({
            'cache': false,
            method: "POST",
            url: '/api/sessions/' + phone,
            data: {code: code.val(), register: register ? 'on' : ''}
        }).done(function (response) {
            $page.removeClass('disabled-mask');
            if (register) {
                window.location.href = '/618';
            } else {
                window.location.href = '/618/thanks';
            }
        }).fail(function (response) {
            $page.removeClass('disabled-mask');
            response.responseJSON.errors.forEach(function (error) {
                $.notify(error, 'error');
            });
        });
    },

    showResponseMessage : function (title) {
        if (title) {
            ResponseMessage.show(title);
        }
    },

    events : function () {
        var self = this;

        $(document).on('click', '[data-profile618-action]', function(e) {
            e.preventDefault();

            var $this = $(this),
                action = $this.data('profile618-action');

            switch (action) {
                case 'get-code-expires-time':
                    $this.prop('disabled', true);
                    $.ajax({
                        type: "POST",
                        url: '/profile/update_code',
                        success: function(response) {
                            if (response) {
                                if (typeof response.refresh !== 'undefined' && response.refresh == 1) {
                                    location.reload();
                                } else if (typeof response.refresh !== 'undefined' && response.refresh == 0) {
                                    var title = $this.data('title');
                                    self.showResponseMessage(title);
                                    $this.prop('disabled', false);
                                }
                            }
                        }
                    });
                    break;

                case 'show-response-message':
                    var title = $this.data('title');
                    self.showResponseMessage(title);
                    break;

                case 'validate-step-1':
                    if (self.validateStep1() && typeof window['login_recaptcha'] !== 'undefined') {
                        grecaptcha.execute( window['login_recaptcha'] );
                    }
                    break;

                case 'validate-step-2':
                    self.validateStep2();
                    break;
            }
        });

        $(self.step1Form).on('submit', function (e) {
            e.preventDefault();
            return false;
        });
        $(self.step2Form + ' input[name="code"]').on('keyup', function(e) {
            var code = window.e ? e.keyCode : e.which;
            if ( code == '13' && $(this).val().replace(/\s/g, '').length ) {
                self.validateStep2();
            }
        });

    }
    
};

$(window).load(function () {
    Profile.init();
    Profile618.init();
});
