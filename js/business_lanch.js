"use strict";

var BusinessLanch = {

    init : function () {
        this.events();
    },

    add : function (element) {
        var $element = $(element),
            id = $element.attr('data-id'),
            selectedProduct = [];

        if ( $element.hasClass('active') ) {
            return true;
        }

        $( ".select_products:checked" ).each(function( index ) {
            selectedProduct.push( $(this).val() );
        });

        BusinessLanch.changeCount(id, {
            format: 'html',
            action: 'increment',
            contains: selectedProduct ? selectedProduct : null
        });
    },

    decrement: function (element) {
        var $element = $(element),
            id = $element.attr("data-id"),
            selectedProduct = [];

        if ( $element.hasClass('disabled') ) {
            return true;
        }

        if (typeof id === 'undefined') {
            return true;
        }

        $( ".select_products:checked" ).each(function( index ) {
            selectedProduct.push( $(this).val() );
        });

        BusinessLanch.changeCount(id, {
            action: 'decrement',
            format: 'html',
            contains: selectedProduct ? selectedProduct : null
        });
    },

    increment: function (element) {
        var $element = $(element),
            id = $element.attr("data-id"),
            selectedProduct = [];

        if (typeof id === 'undefined') {
            return true;
        }

        $( ".select_products:checked" ).each(function( index ) {
            selectedProduct.push( $(this).val() );
        });

        BusinessLanch.changeCount(id, {
            action: 'increment',
            format: 'html',
            contains: selectedProduct ? selectedProduct : null
        });
    },

    changeCount: function (id, data) {
        var $counter = $('.buy_business[data-id="' + id + '"]');

        $counter.addClass('disabled');

        $.post("/api/basket/products/" + id, data,
            function(response){
                console.log(response);
                var cart = $(response.data.cart_html),
                    products = cart.find('.cart-item-list').html(),
                    summary = cart.find('.js-popup-summary').html();
                $('.buy_business[data-id='+id+']').replaceWith(response.data.business_lunch_buy_button_html);
                $('#popup-cart .cart-item-list').html(products);
                $('#popup-cart .js-popup-summary').html(summary);
                $('.header-inner .cart').replaceWith($(response.data.small_cart_html).addClass('animated bounce'));
                $counter.addClass('active').removeClass('disabled');
            }, 'json');
    },

    events : function () {

        $(document).on('click', '.js-add-business-lanch', function (e) {
            e.preventDefault();
            BusinessLanch.add(this);
        });

        $(document).on('click', ".js-increment-business-lanch", function (e) {
            e.preventDefault();
            BusinessLanch.increment(this);
        });

        $(document).on('click', ".js-decrement-business-lanch", function (e) {
            e.preventDefault();
            BusinessLanch.decrement(this);
        });

    }

};

$(window).load(function () {
    BusinessLanch.init();
});