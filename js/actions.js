$(document).ready(function () {

    $(document).on('click', '[data-action]', function(e) {

        e.preventDefault();

        var $this = $(this),
            action = $this.data('action');

        switch (action) {
            case 'js-toggle-block':
                var $target = $($this.data('target'));
                if ( $target.length ) {
                    $target.toggleClass('is-hidden');
                }
                break;

            case 'js-hide-block':
                var $target = $($this.data('target'));
                if ( $target.length ) {
                    $target.hide();
                }
                break;

            case 'show-gallery':
                var name = $this.data('gallery-name'),
                    index = $this.data('index');

                doShowGallery(name, index);
                break;

            default:
                console.log('no action defined');
                break;
        }

    });

});