$(document).ready(function () {

    function dropdownListGoUp() {
        var $list = $('#checkout .new-address .streets-list'),
            $options = $list.find('li'),
            $selected = $options.filter('.selected');

        if ( $selected.index() == 0 ) {
            $options.removeClass('selected');
            $options.eq( $options.length - 1 ).addClass('selected');
            return true;
        }

        $selected.removeClass('selected');
        $options.eq( $selected.index() - 1 ).addClass('selected').focus();
    };

    function dropdownListGoDown() {
        var $list = $('#checkout .new-address .streets-list'),
            $options = $list.find('li'),
            $selected = $options.filter('.selected'),
            $next = $options.eq( $selected.index() + 1 );

        if ( !$next.length ) {
            $options.removeClass('selected');
            $options.eq(0).addClass('selected');
            return true;
        }

        $selected.removeClass('selected');
        $next.addClass('selected').focus();
    };

    $(document)
        .on('street_name.keydown', function (event, keyCode) {
            if ( keyCode == 38 ) {
                dropdownListGoUp();
            } else if ( keyCode == 40 ) {
                dropdownListGoDown();
            } else if ( keyCode == 13 ) {
                var $selected = $('#checkout .new-address .streets-list li.selected');

                if ( $selected.length ) {
                    $selected.trigger('click');
                }
            }
        })
        .on('checkout-show-liqpay-widget', function () {
            $('html').addClass('checkout-show-liqpay-widget');
            window.initCheckoutLiqpayWidget(); //resources\views\checkout\partials\scripts.blade.php
        })
        .on('checkout-change-delivery-date-time-empty', function (event, address) {
            if (address
                && address.street_name
                && window['i18n']
                && window['i18n'].empty_date_time_message_1
                && window['i18n'].empty_date_time_message_2) {
                var address_title = address.street_name;
                if (address.house) {
                    address_title += ' ' + address.house;
                }
                ResponseMessage.show('', window['i18n'].empty_date_time_message_1 + ' ' + address_title + ' ' +  window['i18n'].empty_date_time_message_2 );
            }
        });

});