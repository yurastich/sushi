'use strict';

(function ($) {

    var methods = {

        init : function(options) {
            return this.each(function() {
                var $self = $(this),
                    settings = $.extend({
                        maxFileSize : 2, //mb
                        buttonTarget : '',
                        listTarget : '',
                        $button : null,
                        $list : null,
                        onChange : function (filesData, totalSize) {},
                        onError : function (err) {}
                    }, options),
                    $input = $self,
                    $button, $list;

                if ( settings.buttonTarget === '' ) {
                    $button = $input.closest('.file-upload-container').find('.file-upload-button');
                } else {
                    $button = $(settings.buttonTarget);
                }

                if ( settings.listTarget === '' ) {
                    $list = $input.closest('.file-upload-container').find('.file-upload-list');
                } else {
                    $list = $(settings.listTarget);
                }

                if ( !$button.length || !$list.length ) {
                    console.log('not button or list');
                    return true;
                }

                settings.$button = $button;
                settings.$list = $list;
                $self.data('settings', settings);

                $button.click(function (e) {
                    e.preventDefault();
                    $input.trigger('click');
                });

                $input.change(function () {
                    $self.fileUpload('upload');
                });

                $list.on('click', '[data-action="remove-file"]', function (e) {
                    e.preventDefault();
                    var index = $(this).data('index');
                    $self.fileUpload('remove', index);
                });
            });
        },

        upload : function () {
            var $this = $(this),
                settings = $this.data('settings'),
                $list = settings.$list,
                files = $this[0].files,
                filesData = $this.data('filesData') || {},
                totalSize = $this.data('totalSize') || 0,
                maxFileSize = settings.maxFileSize * 1024 * 1024,
                html = '';

            if ( !$this.attr('multiple') ) {
                filesData = {};
            }

            //console.log(filesData, totalSize);
            for (var i = 0; i < files.length; i++) {
                if ( totalSize + files[i].size > maxFileSize ) {
                    settings.onError({
                        type : 'maxFileSize',
                        value : settings.maxFileSize + ' mb'
                    });
                    break;
                }

                var index = files[i].name + '-' + new Date(),
                    fileTitle = files[i].name;

                totalSize += files[i].size;
                filesData[index] = files[i];

                if ( fileTitle.length > 15 ) {
                    fileTitle = fileTitle.substr(0, 15);
                    fileTitle += '...'+files[i].name.split('.').pop();
                }

                html += '<div data-file-item data-index="' + index + '" class="file-item">';
                html += '<div class="file-item-name">"' + fileTitle + '"</div>';
                html += '<div data-action="remove-file" data-index="' + index + '" class="close-button"></div>';
                html += '</div>';
            }

            $this
                .data('filesData', filesData)
                .data('totalSize', totalSize);

            //console.log(filesData, totalSize);
            if ( $this.attr('multiple') ) {
                $this.val('');
                $list.append( $(html) );
            } else {
                $list.html( $(html) );
            }

            settings.onChange(filesData, totalSize);
        },

        remove : function (index) {
            var $this = $(this),
                settings = $this.data('settings'),
                $list = settings.$list,
                filesData = $this.data('filesData'),
                totalSize = $this.data('totalSize');

            // console.log(filesData, totalSize);
            $list.find('[data-file-item][data-index="' + index + '"]').remove();

            totalSize -= filesData[index].size;
            //filesData.splice(index, 1);
            delete filesData[index];

            $this
                .data('filesData', filesData)
                .data('totalSize', totalSize);

            if ( !$this.attr('multiple') ) {
                $this.val('');
            }

            //console.log(filesData, totalSize);
            settings.onChange(filesData, totalSize);
        },

        getFiles : function () {
            var $this = $(this);

            return $this.data('filesData') || [];
        },

        reset : function () {
            var $this = $(this),
                settings = $this.data('settings'),
                $list = settings.$list;

            $this
                .data('filesData', '')
                .data('totalSize', '')
                .val('');

            $list.empty();
        }

    };

    $.fn.fileUpload = function (method) {
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метод с именем ' +  method + ' не существует для jQuery.fileUpload' );
        }
    };

})(jQuery);