var userRegistrationRecaptcha;

function checkUserRegistrationRecaptcha(response) {
    userRegistrationRecaptcha = response;
    validateHeaderRequestCode();
    return true;
};

function mouseUpHandlerCabinet (e) {
    if ( !is_mobile ) {
        var  container = $(".cabinet"),
            container2 = $(".cabinet-toggle"),
            container3 = $(".cabinet-popup"),
            container4 = $(".cabinet-popup *");

        if (!container.is(e.target) && !container2.is(e.target) && !container3.is(e.target) && !container4.is(e.target) ){ // ... nor a descendant of the container
            $(document).unbind('mouseup touchend', mouseUpHandlerCabinet); //Remove the event listener to prevent memory leaks
            $(".js-header-wrapper .cabinet-popup").hide();
        }
    }
};

function validateHeaderRequestCode () {
    if (userRegistrationRecaptcha && userRegistrationRecaptcha.length) {
        var $this = $(".js-header-wrapper  .js-request-code form .js-btn-submit"),
            $currentForm = $this.closest('form'),
            $container = $currentForm.closest('.js-header-wrapper'),
            $popups = $container.find('.cabinet-popup'),
            $popup_request = $container.find('.js-request-code'),
            $popup_check = $container.find('.js-check-code'),
            phone = $popup_request.find('.phone').val().replace(/[^\d]/g,''),
            success_message = $currentForm.find('.js-success-message').text(),
            error_message = $currentForm.find('.js-error-message').text(),
            $code = $popup_check.find('[name="code"]');

        if ( !phone.length ) {
            $.notify(error_message, 'error');
            return false;
        }

        $currentForm.addClass('disabled-mask');
        return $.ajax({
            'cache': false,
            method: "POST",
            url: '/api/sessions/' + phone,
            data: {recaptcha: userRegistrationRecaptcha}
        }).done(function (response) {
            $currentForm.removeClass('disabled-mask');
            //alert("afds");
            dataLayer.push({ 'event': 'UAEvent', 'evenCategory': 'Авторизация'});
            if (typeof response.data.user !== 'undefined') {
                location.reload();
            } else {
                $.notify(success_message, 'success');
                $popups.not($popup_check).hide().attr('data-is-visible', false);
                $popup_check.show().attr('data-is-visible', true);
                $code.focus();
                if (typeof response.data.register_form !== 'undefined' && response.data.register_form) {
                    $popup_check.find('form').append(response.data.register_form);
                }
            }

        }).fail(function (response) {
            $currentForm.removeClass('disabled-mask');

            response.responseJSON.errors.forEach(function (error) {
                $.notify(error, 'error');
            });
        });
    }
};

$(function () {

    $(".js-header-wrapper .js-cabinet-toggle").click(function (e) {
        var $this = $(this),
            $container = $this.closest('.js-header-wrapper');

        if ( $this.attr('href') ) {
            e.preventDefault();
        }

        $popupVisible = $container.find('.cabinet-popup').filter('[data-is-visible="true"]');
        $popupVisible.toggle();

        if( $popupVisible.is( ":visible" ) ){
            $(document).bind('mouseup touchend', mouseUpHandlerCabinet );
            $popupVisible.find('input').eq(0).focus();
        }else{
            $(document).unbind('mouseup touchend', mouseUpHandlerCabinet );
        }
    });

    $(".js-header-wrapper  .js-request-code form .js-btn-submit").click(function (e) {
        e.preventDefault();
        validateHeaderRequestCode();
    });

    $(".js-header-wrapper  .js-check-code form").on('submit', function (e) {
        e.preventDefault();

        var $this = $(this),
            $container = $this.closest('.js-header-wrapper'),
            $popups = $container.find('.cabinet-popup'),
            $popup_request = $container.find('.js-request-code'),
            $popup_register = $container.find('.js-register-user'),
            $register_user_form = $container.find('.js-register-user form'),
            $popup_check = $container.find('.js-check-code'),
            phone = $popup_request.find('input[name="phone"]').val().replace(/[^\d]/g,''),
            register = $popup_check.find('input[name="confirm"]').val(),
            error_message = $this.find('.js-error-message').text(),
            code = $this.find('input[name="code"]');
            register = $this.find('input[name="confirm"]').val();

        if (!code.val().length) {
            $.notify(error_message, 'error');
            return;
        }

        $this.addClass('disabled-mask');

        return $.ajax({
            'cache': false,
            method: "POST",
            url: '/api/sessions/' + phone,
            data: {code: code.val(), register: register}
        }).done(function (response) {
            $this.removeClass('disabled-mask');

            if (typeof response.data.user !== 'undefined') {
                $popups.hide().attr('data-is-visible', false);
                if (typeof register !== 'undefined' && register == 'on' && typeof response.data.redirect_to !== 'undefined') {
                    window.location.href = response.data.redirect_to;
                } else {
                    location.reload();
                }
            } else {
                $.notify(response.data.message, 'success');
                code.val('');
                phone.val('');

                $popups.hide().attr('data-is-visible', false);
                $popup_check.find('.user-cabinet-register-form').remove();
                $container.find('.js-cabinet-toggle').trigger('click');
                grecaptcha.reset();
            }
        }).fail(function (response) {
            $this.removeClass('disabled-mask');

            response.responseJSON.errors.forEach(function (error) {
                $.notify(error, 'error');
            });
        });
    });

    // $(".js-header-wrapper .js-register-user form").on('submit', function (e) {
    //     e.preventDefault();
    //
    //     var $this = $(this),
    //         $container = $this.closest('.js-header-wrapper'),
    //         $popup_request = $container.find('.js-request-code'),
    //         phone = $popup_request.find('input[name="phone"]').val().replace(/[^\d]/g,''),
    //
    //         name = $this.find('input[name="name"]').val(),
    //         city = $this.find('select[name="city"]').val(),
    //         gender = $this.find('select[name="gender"]').val(),
    //
    //         currentDate = new Date(),
    //         birthdayDate = parseInt($this.find('input[name="birthday[date]"]').val()),
    //         birthdayMonth = parseInt($this.find('input[name="birthday[month]"]').val()),
    //         birthdayYear = parseInt($this.find('input[name="birthday[year]"]').val()),
    //
    //         errorNameMessage = $this.find('.js-error-name-message').text(),
    //         errorCityMessage = $this.find('.js-error-city-message').text(),
    //         errorBirthdayMessage = $this.find('.js-error-birthday-message').text(),
    //         errorGenderMessage = $this.find('.js-error-gender-message').text();
    //
    //     if (!name.length) {
    //         $.notify(errorNameMessage, 'error');
    //         return;
    //     }
    //
    //     if (!city.length) {
    //         $.notify(errorCityMessage, 'error');
    //         return;
    //     }
    //
    //     if ( !birthdayDate || !birthdayMonth || !birthdayYear ) {
    //         $.notify(errorBirthdayMessage, 'error');
    //         return;
    //     }
    //     if ( birthdayDate > 31 || birthdayMonth > 12 || (birthdayYear < currentDate.getFullYear() - 150) || (birthdayYear > currentDate.getFullYear() - 3) ) {
    //         $.notify(errorBirthdayMessage, 'error');
    //         return;
    //     }
    //
    //     if (!gender.length) {
    //         $.notify(errorGenderMessage, 'error');
    //         return;
    //     }
    //
    //     $this.addClass('disabled-mask');
    //
    //     return $.ajax({
    //         'cache': false,
    //         method: "POST",
    //         url: '/api/sessions/' + phone,
    //         data: {register: $this.serialize()}
    //     }).done(function (response) {
    //         $this.removeClass('disabled-mask');
    //
    //         if (typeof response.data.user !== 'undefined') {
    //             location.reload();
    //         }
    //     }).fail(function (response) {
    //         $this.removeClass('disabled-mask');
    //
    //         response.responseJSON.errors.forEach(function (error) {
    //             $.notify(error, 'error');
    //         });
    //     });
    // });

});