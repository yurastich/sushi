var Popup = {
	init: function() {
		this.events();
	},
	events: function(){
		jQuery('[data-popup]').click(function(e) {
			e.preventDefault();
			Popup.show( "popup-" + jQuery(this).data('popup') );

            $('form[name="reviews"]')[0].reset();
            $('form[name="reviews"]').show();
		});
		$(".popup-close, [data-popup-close]").click(function(e){
			e.preventDefault();
			Popup.hide( jQuery(this).closest('.popup').attr('id') );
		});
		$('.popup .popup-container').click(function(e) {
			e.stopPropagation();
		});
		$('.popup').click(function(e) {
			e.stopPropagation();
			Popup.hide( jQuery(this).attr('id') );
		});
	},
	show: function(id){
        var $popup = jQuery("#" + id),
            $container = $popup.find('.popup-container'),
            show_class_name = $container.data('show-class-name');

		jQuery('body').addClass('popup-open');
		//$popup.find('.popup-container').css('top','-100%')
		//$popup.show();
        $container.addClass(show_class_name);
        $popup.addClass('active');
		//if($(document).width() > 767) {
		//	$container.animate({
		//		top: "80px"
		//	}, 250, function () {
		//		// Animation complete.
		//	});
		//}else{
		//	$container.animate({
		//		top: "60px"
		//	}, 250, function () {
		//		// Animation complete.
		//	});
		//}
	},
	hide: function(id){
        var $popup = jQuery("#" + id),
            $container = $popup.find('.popup-container'),
            show_class_name = $container.data('show-class-name'),
            hide_class_name = $container.data('hide-class-name');

		jQuery('body').removeClass('popup-open');
		//$popup.hide();
        $container.removeClass(show_class_name).addClass(hide_class_name);

        setTimeout(function() {
            $popup.removeClass('active');
            $container.removeClass(hide_class_name);
            $popup.find('iframe').attr('src','');
            $popup.trigger("hide.popup");
        }, 500);

	},
	hideAll: function(){
		jQuery('body').removeClass('popup-open');
		//jQuery(".popup").hide();
        jQuery(".popup").removeClass('active');
	}
}
jQuery(function() {
	Popup.init();
});
