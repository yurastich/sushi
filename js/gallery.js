'use strict';

function doCreateGallery(name, index) {
    var $popup = $('[data-gallery-popup="' + name + '"]'),
        $photo_slider = $popup.find('[data-js-gallery-photo-slider]'),
        photo_slider_html = '',
        images = {};

    if ( index == undefined ) {
        index = 0;
    }

    $('[data-gallery-name="' + name + '"]').each(function (index, element) {
        var $element = $(element);
        images[index] = {};
        images[index].title = $element.data('title');
        images[index].original = $element.data('original-src');
    });

    $.each( images, function( key, value ) {
        photo_slider_html += '<div>';
        photo_slider_html += '<div class="gallery-photo-item">';
        photo_slider_html += '<img src="' + value.original + '" width="800" height="800" title="' + value.title + '">';
        photo_slider_html += '</div>';
        photo_slider_html += '</div>';
    });

    $photo_slider.html( $(photo_slider_html) );
    $popup.addClass('is-loaded');
    doInitGallery(name, index);
};

function doShowGallery(name, index) {
    var $popup = $('[data-gallery-popup="' + name + '"]');

    if ( $popup.hasClass('is-loaded') ) {
        doInitGallery(name, index);
    } else {
        doCreateGallery(name, index);
    }
};

function doInitGallery(name, index) {
    var $popup = $('[data-gallery-popup="' + name + '"]'),
        popup_id = $popup.attr('id'),
        $photo_slider = $popup.find('[data-js-gallery-photo-slider]');

    if ( index == undefined ) {
        index = 0;
    }

    if ( $photo_slider.length && !$photo_slider.hasClass('.slick-initialized') ) {
        $photo_slider.slick({
            infinite: true,
            slidesToShow: 1,
            initialSlide: index,
            arrows: true,
            dots: false
        });

        Popup.show(popup_id);
        $(window).resize();
    }
};

function doDestroyGallery($photo_slider) {
    if ( !$photo_slider.length ) {
        return false;
    }

    $photo_slider.slick('unslick');
};

$(document).ready(function () {

    $('[data-gallery-popup]').on('hide.popup', function () {
        var $photo_slider = $(this).find('[data-js-gallery-photo-slider]');
        doDestroyGallery($photo_slider);
    });

});
