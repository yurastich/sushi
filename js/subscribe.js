$('form[name="subscribe"]').validate({
    rules: {
        name: {
            required: true
        },
        email: {
            required: true,
            email: true
        }
    },
    errorPlacement : function(error, element) {},
    submitHandler: function(form) {
        $.post(getUrlLangPrefix() + "/subscribe-add", {data : $('form[name="subscribe"]').serialize() },
            function(data){
                if (data.status == 'success') {
                    ecommerceSendSubscribe(); //public/js/ecommerce.js
                    $('form[name="subscribe"] .errors').hide();
                    $('form[name="subscribe"]').html('<div style="text-align: center; width: 100%">' + data.message + '</div>');
                } else {
                    $('form[name="subscribe"] .errors').html(data.message).show();
                }
            }, 'json');

    }
});