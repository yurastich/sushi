"use strict";

var Basket =
{
    init: function () {
        $('body').on('click', '.js-add-cart', function (e) {
            Basket.add(this, Basket.getList(this));
        });
        $('body').on('click', '.js-increment-cart', function (e) {
            Basket.increment(this, Basket.getList(this));
        });
        $('body').on('click', '.js-decrement-cart', function (e) {
            Basket.decrement(this, Basket.getList(this));
        });
        $('body').on('click', '.js-remove-from-cart', function (e) {
            Basket.removeItem(this, Basket.getList(this));
        });

    },

    getList: function (el) {
        var list = window['ecommerceList']['catalog'];

        if ( el && $(el).length && $(el).closest('.recommended').length ) {
            list = window['ecommerceList']['recom']; //resources/views/home/partials/recommended.blade.php
        }

        return list;
    },

    add: function (element, list) {
        if (!this.validateCart()) {
            return false;
        }

        var id_product = $(element).attr("data-id");
        var $animate = $(element).closest('.product-item').find('.animate-product-item');

        //console.log(category);
        if (typeof id_product === 'undefined') return true;
        if ($(element).hasClass('active')) return true;
        var giftId = $('.js-selected-gift-' + id_product + " [data-product-id]").attr('data-product-id');
        // if (typeof ga !== 'undefined') {
        //     ga('send', {
        //         hitType: 'event',
        //         eventCategory: 'Products',
        //         eventAction: 'buy',
        //         eventLabel: id_product
        //     });
        // }
        Basket.changeCount(id_product, {
            action: 'increment',
            format: 'html',
            list: list,
            contains: giftId ? giftId.split(',') : null
        }).done(function(){
            $(element).addClass('active');
            $animate.addClass('active'); //animate product item
        });
    },

    decrement: function (element, list) {
        if (!this.validateCart()) {
            return false;
        }

        var id_product = $(element).attr("data-id");
        if ($(element).hasClass("disabled")) return true;
        if (typeof id_product === 'undefined') return true;
        var contains = $(element).attr("data-contain-ids");
        if (!contains) {
            contains = $('.js-selected-gift-' + id_product + " [data-product-id]").attr('data-product-id');
        }
        Basket.changeCount(id_product, {
            action: 'decrement',
            format: 'html',
            list: list,
            contains: contains ? contains.split(',') : null
        });
    },

    increment: function (element, list) {
        if (!this.validateCart()) {
            return false;
        }

        var id_product = $(element).attr("data-id");
        if (typeof id_product === 'undefined') return true;
        var contains = $(element).attr("data-contain-ids");
        if (!contains) {
            contains = $('.js-selected-gift-' + id_product + " [data-product-id]").attr('data-product-id');
        }
        Basket.changeCount(id_product, {
            action: 'increment',
            format: 'html',
            list: list,
            contains: contains ? contains.split(',') : null
        });
    },

    removeItem: function (element, list) {
        if (!this.validateCart()) {
            return false;
        }

        var id_product = $(element).attr("data-id");
        if (typeof id_product === 'undefined') return true;
        var contains = $(element).attr("data-contain-ids");
        var current_qty = $(element).data('current-quantity'); //for ecommerce

        Basket.changeCount(id_product, {
            current_qty: current_qty,
            qty: 0,
            format: 'html',
            list: list,
            contains: contains ? contains.split(',') : null
        });
    },

    changeCount: function (id, data) {
        var self = this,
            localePrefix = window.mafia_locale ? '/' + window.mafia_locale : '';

        data.picture = {w: 100, h: 100};
        return $.ajax({
            'cache': false,
            method: "POST",
            url: localePrefix + "/api/basket/products/" + id,
            data: data
        }).done(function (response) {
            var cart = $(response.data.cart_html);
            var products = cart.find('.cart-item-list').html();
            var productIds = $(products).find('[data-product-id]').map(function(key, item){return $(item).attr('data-product-id')}).toArray();
            $('.js-category-buy-button[data-id='+id+']').replaceWith(response.data.category_buy_button_html);
            $('.js-product-buy-button[data-id='+id+']').replaceWith(response.data.product_buy_button_html);
            $('.buy_business[data-id='+id+']').replaceWith(response.data.business_lunch_buy_button_html);
            $('.js-additional-buy-button[data-id='+id+']').replaceWith(response.data.additional_buy_button_html);
            //Резет кнопки купить если товар не в корзине
            $('.js-add-cart').each(function(key, item){
                if (!($.inArray($(item).attr('data-id'), productIds ) > -1)) {
                    $(item).removeClass('active');
                }
            });
            if ($(response.data.small_cart_html).find('.empty').length) {
                $(document).trigger('cart.empty');
            }
            var summary = cart.find('.js-popup-summary').html();

            $('#popup-cart .cart-item-list').html(products);
            $('#popup-cart .js-popup-summary').html(summary);
            $('.header-inner .cart').replaceWith($(response.data.small_cart_html).addClass('animated bounce'));

            self.updateProductAdditionals(response);
            ecommerceUpdateCart(id, data, response); //public/js/ecommerce.js
        }).fail(function (response) {
            var type = 'error';
            if (response.responseJSON && response.responseJSON.errors) {
                var arrayLength = response.responseJSON.errors.length;
                for (var i = 0; i < arrayLength; i++) {
                    // if (response.status === 428) {
                    //     type = 'success';
                    // }
                    $.notify(response.responseJSON.errors[i], type);
                }
            } else {
                $.notify(window.i18n.general_error, type);
            }
        });
    },

    validateCart: function () {
        if ( !$('.js-header-cart').length ) {
            var $msg = $('#js-header-cart-msg');
            if ($msg.length && $msg.text().length) {
                ResponseMessage.show('', $msg.text());
            }
            return false;
        } else {
            return true;
        }
    },

    updateProductAdditionals: function (data) {
        if (data && data['productAdditions'] && Object.keys(data['productAdditions']).length) {
            for (var key in data['productAdditions']) {
                var el = data['productAdditions'][key];

                if (el['id'] && el['quantity']) {
                    $('[data-product-id="' + el['id'] + '"]').find('input').val(el['quantity']);
                }
            }
        }
    }

};

$(window).load(function () {
    Basket.init();
});
